import 'package:cooking_mama/model/UserModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';

class FirebaseHelper {
  final auth = FirebaseAuth.instance;

  Future<UserModel> handleSignIn(String mail, String mdp) async {
    final UserModel user =
        (await auth.signInWithEmailAndPassword(email: mail, password: mdp)).user as UserModel;
    return user;
  }

  Future<UserModel> create(
      String mail, String mdp, String prenom, String nom) async {
    final create = await auth.createUserWithEmailAndPassword(email: mail, password: mdp);
    final UserModel user = create.user as UserModel;
    String uid = user.uid;
    Map<String, String> map = {"prenom": prenom, "nom": nom, "uid": uid};
    addUser(uid, map);
    return user;
  }

  static final entryPoint =  FirebaseDatabase.instance.reference();
  final entry_user = entryPoint.child("users");

  addUser(String uid, Map map) {
    entry_user.child(uid).set(map);
  }

  Future<UserModel> getUser(String uid) async {
    DataSnapshot snapshot = await entry_user.child(uid).once();
    UserModel user = UserModel(snapshot);
    return user;
  }
}
