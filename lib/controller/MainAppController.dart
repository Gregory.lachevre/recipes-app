import 'dart:ui';

import 'package:cooking_mama/model/FirebaseHelper.dart';
import 'package:cooking_mama/model/UserModel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'PokemonController.dart';

class MainAppController extends StatefulWidget {
  MainAppControllerState createState() => MainAppControllerState();
}

class MainAppControllerState extends State<MainAppController> {
  UserModel currentUser;

  User user = FirebaseHelper().auth.currentUser;

  @override
  void initState() {
    super.initState();
    FirebaseHelper().getUser(user.uid).then((currentUser) {
      setState(() {
        this.currentUser = currentUser;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("pokedex de: ${user.email}"),
          backgroundColor: Colors.blueAccent,
        ),
        body: Center(
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image:
                      new ExactAssetImage('assets/launcher/backgroundMenu.png'),
                  fit: BoxFit.cover),
            ),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset("assets/launcher/enteteMenu.png"),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomMenu(
                            asset: "assets/launcher/PokedexMenu.png",
                            text: "Pokedex",
                            context: context,
                            action: "POKEDEX"),
                        Spacer(),
                        CustomMenu(
                          asset: "assets/launcher/SacMenu.png",
                          text: "Sac",
                        ),
                        Spacer(),
                        CustomMenu(
                          asset: "assets/launcher/PokeballMenu.png",
                          text: "Pokemon",
                        ),
                        Spacer(),
                        CustomMenu(
                          asset: "assets/launcher/SDDQMENU.png",
                          text: "Outils",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  CustomMenu({String asset, String text, String action, BuildContext context}) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            switch (action) {
              case "POKEDEX":
                Navigator.push(
                    context,
                    PageRouteBuilder(
                      transitionDuration: Duration(seconds: 2),
                      transitionsBuilder: (context,animation,animationTime,child) {
                        return ScaleTransition(
                          alignment: Alignment.center,
                          scale: animation,
                          child: child,
                        );
                      },
                      pageBuilder: (context,animation,animationTime) {
                      return PokemonController();
                      }

                ));
                break;
              case "SAC":
                break;
              case "POKEMONS":
                break;
              case "OUTILS":
                break;
              default:
                break;
            }
          },
          child: Container(
              width: 90,
              height: 90,
              child: Image.asset(
                asset,
                fit: BoxFit.cover,
              )),
        ),
        Text(
          text,
          style: TextStyle(
              fontFamily: 'Pokemon', fontSize: 24, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
