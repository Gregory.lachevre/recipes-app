import 'package:cooking_mama/model/Pokemon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PokeDetail extends StatelessWidget {
  final Pokemon pokemon;

  PokeDetail({this.pokemon});

  bodyWidget(BuildContext context) => Stack(
        children: <Widget>[
          Positioned(
            height: MediaQuery.of(context).size.height / 1.5,
            width: MediaQuery.of(context).size.width - 20,
            left: 10.0,
            top: MediaQuery.of(context).size.height * 0.1,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(
                    height: 100.0,
                  ),
                  Text(
                    pokemon.name,
                    style: TextStyle(
                        fontFamily: 'Pokemon',
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Text("Height: ${pokemon.height}",
                      style: TextStyle(fontFamily: 'Pokemon', fontSize: 22.0)),
                  Text("Weight: ${pokemon.weight}",
                      style: TextStyle(fontFamily: 'Pokemon', fontSize: 22.0)),
                  Text(
                    "Types",
                    style: TextStyle(
                        fontFamily: 'Pokemon',
                        fontSize: 22.0,
                        fontWeight: FontWeight.bold),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: pokemon.type
                        .map((type) => FilterChip(
                            backgroundColor: Colors.amber,
                            label: Text(type,
                                style: TextStyle(
                                    fontFamily: 'Pokemon', fontSize: 22.0)),
                            onSelected: (b) {}))
                        .toList(),
                  ),
                  Text("Weakness",
                      style: TextStyle(
                          fontFamily: 'Pokemon',
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: pokemon.weaknesses
                        .map((weakness) => FilterChip(
                            backgroundColor: Colors.red,
                            label: Text(
                              weakness,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'Pokemon',
                                  fontSize: 22.0),
                            ),
                            onSelected: (b) {}))
                        .toList(),
                  ),
                  Text("Next Evolution",
                      style: TextStyle(
                          fontFamily: 'Pokemon',
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: pokemon.nextEvolution == null
                        ? <Widget>[Text("This is the final form")]
                        : pokemon.nextEvolution
                            .map((nextEvolution) => FilterChip(
                                  backgroundColor: Colors.green,
                                  label: Text(
                                    nextEvolution.name,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'Pokemon',
                                        fontSize: 22.0),
                                  ),
                                  onSelected: (b) {},
                                ))
                            .toList(),
                  )
                ],
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: Hero(
                tag: pokemon.img,
                child: Container(
                  height: 100.0,
                  width: 100.0,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      image: DecorationImage(
                          fit: BoxFit.cover, image: NetworkImage(pokemon.img))),
                )),
          )
        ],
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.red,
        title: Text(pokemon.name),
      ),
      body: bodyWidget(context),
    );
  }
}
