import 'package:cooking_mama/model/FirebaseHelper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogController extends StatefulWidget {
  LogControllerState createState() => LogControllerState();
}

class LogControllerState extends State<LogController> {
  String _adresseMail;
  String _password;
  String _prenom;
  String _nom;
  bool _log = true;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Authentification"),),
      body: new SingleChildScrollView(
        child: new Column(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.all(20.0),
              width: MediaQuery.of(context).size.width - 40,
              height: MediaQuery.of(context).size.height / 2,
              child: new Card(
                  elevation: 8.5,
                  child: new Container(
                    margin: EdgeInsets.only(left: 5.0, right: 5.0),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: textfields(),
                    ),
                  )
              ),
            ),
            new RaisedButton(
              onPressed: _handleLog,
              color: Colors.blue,
              child: new Text((_log == true)? "Se connecter": "S'inscrire",
                style: new TextStyle(
                    color: Colors.white,
                    fontSize: 20.0
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _handleLog() {
    if (_adresseMail != null) {
      if (_password != null) {
        if (_log == true) {
          // Se connecter
          FirebaseHelper().handleSignIn(_adresseMail, _password).then(( user) {
            print("Nous avons un user");
          }).catchError((error) {
            alert(error.toString());
          });
        } else {
          if (_prenom != null) {
            if (_nom != null) {
              // Créer un compte avec les données de l'utilisateur
              FirebaseHelper().create(_adresseMail, _password, _prenom, _nom).then(( user) {
                print("Nous avons pu créer un utilisateur");
              }).catchError((error) {
                alert(error.toString());
              });
            } else {
              // Alerte nom
              alert("Pour finaliser votre inscription, veuillez entrer votre nom");
            }
          } else {
            // Alerte Prénom
            alert("Veuillez entrer un prénom pour continuer");
          }
        }
      } else {
        //Alerte Pass
        alert("Le mot de passe est vide");
      }
    } else {
      // Alerte Mail
      alert("L'adresse mail est vide");
    }
  }

  List<Widget> textfields() {
    List<Widget> widgets = [];

    widgets.add(TextField(
      decoration: InputDecoration(hintText: "Adresse mail"),
      onChanged: (string) {
        setState(() {
          _adresseMail = string;
        });
      },
    ));

    widgets.add(TextField(
      decoration: InputDecoration(hintText: "Mot de pasee"),
      obscureText: true,
      onChanged: (string) {
        setState(() {
          _password = string;
        });
      },
    ));

    if (!_log) {
      widgets.add(TextField(
          decoration: InputDecoration(hintText: "Prénom"),
          onChanged: (string) {
            setState(() {
              _prenom = string;
            });
          }));

      widgets.add(TextField(
          decoration: InputDecoration(hintText: "Nom"),
          onChanged: (string) {
            setState(() {
              _nom = string;
            });
          }));
    }
    widgets.add(
        new FlatButton(
            onPressed: () {
              setState(() {
                _log = !_log;
              });
            },
            child: new Text(
                (_log == true)
                    ? "Pour créer un compte, appuyez ici"
                    : "Vous avez déjà un compte? appuyez ici"
            )
        )
    );
    return widgets;
  }

  Future<void> alert(String message) async {
    Text title = Text("Erreur");
    Text msg = Text(message);
    FlatButton okButton = FlatButton(onPressed: ()=> Navigator.of(context).pop(),
      child: Text("OK"),
    );
    return showDialog(
        context: context,
        barrierDismissible: false,
      builder: (BuildContext ctx) {
        return (Theme.of(context).platform == TargetPlatform.iOS)
        ? CupertinoAlertDialog(title: title, content: msg, actions: <Widget>[okButton],)
        : AlertDialog(title: title, content: msg, actions: <Widget>[okButton]);
      }
    );

  }


}
